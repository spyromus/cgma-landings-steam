module Locomotive
  module Steam
    module Liquid
      module Drops
        class SharedContentTypes < ::Liquid::Drop

          def liquid_method_missing(meth)
            if content_type = fetch_content_type(meth.to_s)
              SharedContentEntryCollection.new(content_type)
            else
              nil
            end
          end

          private

          def repository
            @context.registers[:services].repositories.shared_content_type
          end

          def fetch_content_type(slug)
            @content_type_map ||= {}

            if !@content_type_map.include?(slug)
              @content_type_map[slug] = repository.by_slug(slug)
            end

            @content_type_map[slug]
          end

        end

      end
    end
  end
end
